# Incident Manager

This project defines the required resouces to implement a response plan which has a runbook attached to trigger and get the status of gitlab's pipeline.

The project has .json files which contain the policies attached to aws roles and aws iam groups. Also, there is a .yaml file which contain the python3 script code used by the runbook.

The main.tf file defines the aws configuration resource as follow: 

1. Creating required role to run runbook automation.
2. adding AmazonSSMAutomationRole policy to role_runbook_execution.
3. creating required role to run runbook automation from incident manager's response plan.
4. adding custom policies to assume the required role to invoked the runbook from incident manager's response plan.
5. creating system manager document automation.
6. creating contacts.
7. creating contact channels.
8. Creating the escalation plan.
9. adding stage to the escalation plan.
10. creating On-call Schedule calendar.
11. creating incident response plan.
12. Creating IAM user group to access to incident manager dashboard.
13. Attaching policies to IAM user group.
14. adding policy IAMUserChangePassword to group.
15. creating an IAM user.
16. attaching IAM user to IAM user group.
17. Enable access console to that user creating a encrypted password.
18. creating lambda's function role.
19. adding AWSLambdaExecute  policy to lambda role.
20. packaging the python code to .zip
21. creating lambda function.
creating cloudwatch log group for the function.
22. creating the api.
23. deploy the api.
24. integrating http endpoint with lambda function.
25. attaching permissions to allow api gateway execute lambda functions.
26. creating log metric filter.
27. creating alarm.