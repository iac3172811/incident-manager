terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.40.0"
    }
  }
  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "us-east-1"
}

locals {
  runbook_yaml_content       = file("runbook-content.yaml")
  trust_policy               = file("trust_policy_role.json")
  trust_policy_response_plan = file("trust_policy_role_response_plan.json")
  response_plan_policy       = file("response-plan-policy.json")
  rotation_recurrence        = file("recurrence-content.json")
  incident_manager_policy    = file("policy-groups.json")
  trust_policy_lambda = file("trust_policy_role_lambda.json")
}

// creating required role to run runbook automation
resource "aws_iam_role" "role_runbook_execution" {
  name               = "role_runbook_execution"
  description        = "this role gives permission to execute ssm automation documents"
  assume_role_policy = local.trust_policy
}

//adding AmazonSSMAutomationRole policy to role_runbook_execution
resource "aws_iam_role_policy_attachment" "attach-ssm-automation-policy-role-runbook" {
  role       = aws_iam_role.role_runbook_execution.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonSSMAutomationRole"
}

// creating required role to run runbook automation from incident manager's response plan
resource "aws_iam_role" "response_plan_execution_role" {
  name               = "response_plan_execution_role"
  description        = "AWS Systems Manager Incident Manager Start SSM Automation Document Role"
  assume_role_policy = local.trust_policy_response_plan
}

//adding custom policies to assume the required role to invoked the runbook from incident manager's response plan
resource "aws_iam_role_policy" "response_plan_policy" {
  name   = "AWS-System-Manager-Incident-Manager-Automation-Policy-response-plan"
  role   = aws_iam_role.response_plan_execution_role.id
  policy = local.response_plan_policy
}

//creating system manager document automation
resource "aws_ssm_document" "response_plan_automation" {
  name            = "response_plan"
  document_format = "YAML"
  document_type   = "Automation"
  content         = local.runbook_yaml_content
}

// creating contacts
resource "aws_ssmcontacts_contact" "personal-contact" {
  alias = "personal-contact"
  type  = "PERSONAL"
}

resource "aws_ssmcontacts_contact" "support-contact" {
  alias = "noc-contacts"
  type  = "PERSONAL"
}

//creating contact channels
resource "aws_ssmcontacts_contact_channel" "personal-contact-channel" {
  contact_id = aws_ssmcontacts_contact.personal-contact.id
  delivery_address {
    simple_address = "jperez.julian13+alias1@gmail.com"
  }
  name = "Julian-email"
  type = "EMAIL"
}

resource "aws_ssmcontacts_contact_channel" "support-contact-channel-1" {
  contact_id = aws_ssmcontacts_contact.support-contact.id
  delivery_address {
    simple_address = "operators.payu@payu.com"
  }
  name = "Operator-email-1"
  type = "EMAIL"
}

resource "aws_ssmcontacts_contact_channel" "support-contact-channel-2" {
  contact_id = aws_ssmcontacts_contact.support-contact.id
  delivery_address {
    simple_address = "andreamedina14@hotmail.com"
  }
  name = "Operator-email-2"
  type = "EMAIL"
}

// Creating the escalation plan
resource "aws_ssmcontacts_contact" "escalation_plan" {
  alias = "escalation-plan-1"
  type  = "ESCALATION"
}

// adding stage to the escalation plan
resource "aws_ssmcontacts_plan" "escalation_plan" {
  contact_id = aws_ssmcontacts_contact.escalation_plan.id
  stage {
    duration_in_minutes = 20
    target {
      contact_target_info {
        is_essential = true
        contact_id   = aws_ssmcontacts_contact.personal-contact.id
      }
    }
  }

  stage {
    duration_in_minutes = 0
    target {
      contact_target_info {
        is_essential = true
        contact_id   = aws_ssmcontacts_contact.support-contact.id
      }
    }
  }
}

// creating On-call Schedule calendar
/*
resource "aws_ssmcontacts_contact" "stc-1" {
  alias = "stc"
  type  = "ONCALL_SCHEDULE"
}
*/

resource "aws_ssmcontacts_rotation" "rotation" {
  contact_ids = [
    aws_ssmcontacts_contact.support-contact.arn
  ]
  name         = "stc-rotation"
  time_zone_id = "America/Bogota"
  start_time   = "2024-04-01T00:00:00Z"
  recurrence {
    number_of_on_calls = 1
    recurrence_multiplier = 2
    shift_coverages {
      coverage_times {
        end {
          hour_of_day = 0
          minute_of_hour = 0
        }
        start {
          hour_of_day = 0
          minute_of_hour = 0
        }
      }
      map_block_key = "SAT"
    }

    shift_coverages {
      coverage_times {
        end {
          hour_of_day = 0
          minute_of_hour = 0
        }
        start {
          hour_of_day = 0
          minute_of_hour = 0
        }
      }
      map_block_key = "SUN"
    }

    shift_coverages {
      coverage_times {
        end {
          hour_of_day = 0
          minute_of_hour = 0
        }
        start {
          hour_of_day = 0
          minute_of_hour = 0
        }
      }
      map_block_key = "MON"
    }

    shift_coverages {
      coverage_times {
        end {
          hour_of_day = 0
          minute_of_hour = 0
        }
        start {
          hour_of_day = 0
          minute_of_hour = 0
        }
      }
      map_block_key = "TUE"
    }

    shift_coverages {
      coverage_times {
        end {
          hour_of_day = 0
          minute_of_hour = 0
        }
        start {
          hour_of_day = 0
          minute_of_hour = 0
        }
      }
      map_block_key = "WED"
    }

    shift_coverages {
      coverage_times {
        end {
          hour_of_day = 0
          minute_of_hour = 0
        }
        start {
          hour_of_day = 0
          minute_of_hour = 0
        }
      }
      map_block_key = "THU"
    }

    shift_coverages {
      coverage_times {
        end {
          hour_of_day = 0
          minute_of_hour = 0
        }
        start {
          hour_of_day = 0
          minute_of_hour = 0
        }
      }
      map_block_key = "FRI"
    }

    weekly_settings {
      day_of_week = "FRI"
      hand_off_time {
        hour_of_day = 0
        minute_of_hour = 0
      }
    }
  }
}

// creating incident response plan
resource "aws_ssmincidents_response_plan" "response_plan_1" {
  name         = "response_plan_1"
  display_name = "response_plan_1"
  engagements = [
    aws_ssmcontacts_plan.escalation_plan.id
  ]

  incident_template {
    title   = "Incoming alert by api gateway"
    impact  = "4"
    summary = "Trigger gitlab pipeline executing a runbook"
  }
  action {
    ssm_automation {
      document_name = aws_ssm_document.response_plan_automation.name
      role_arn      = aws_iam_role.response_plan_execution_role.arn
      parameter {
        name   = "key"
        values = ["value1", "value2"]
      }
      dynamic_parameters = {
        someKey    = "INVOLVED_RESOURCES"
        anotherKey = "INCIDENT_RECORD_ARN"
      }
    }
  }
}

// Creating IAM user group to access to incident manager dashboard
resource "aws_iam_group" "incident_manager_group" {
  name = "incident_manager_group"
}

// Attaching policies to IAM user group
resource "aws_iam_group_policy" "group_policy-1" {
  name   = "AWS-System-Manager-Incident-Manager-Automation-Policy"
  group  = aws_iam_group.incident_manager_group.id
  policy = local.response_plan_policy
}

resource "aws_iam_group_policy" "group_policy-2" {
  name   = "AWS-IncidentManager-Contacts"
  group  = aws_iam_group.incident_manager_group.id
  policy = local.incident_manager_policy
}

// adding policy IAMUserChangePassword to group
resource "aws_iam_group_policy_attachment" "attach-ssm-automation-policy-role-runbook" {
  group      = aws_iam_group.incident_manager_group.id
  policy_arn = "arn:aws:iam::aws:policy/IAMUserChangePassword"
}

// creating an IAM user
resource "aws_iam_user" "operator_user" {
  name = "operator"
}

// attaching IAM user to IAM user group
resource "aws_iam_group_membership" "add-users" {
  name = "incident_manager_group"
  users = [
    aws_iam_user.operator_user.name
  ]
  group = aws_iam_group.incident_manager_group.name
}

/* Optional

// Enable access console to that user creating a encrypted password
resource "aws_iam_user_login_profile" "operator_login" {
  user    = aws_iam_user.operator_user.name
  pgp_key = "keybase:jperezjulian13gm" //replace your keybase username (if you do not have one, create one before)
  password_reset_required = true
}

output "password" {
  value = aws_iam_user_login_profile.operator_login.encrypted_password
}

//to decrypt use this command to get the password:
// install keybase
// keybase login
// echo "password" | base64 --decode | keybase pgp decrypt
//copy the password and go to log in to access to aws console
*/


// creating lambda's function role
resource "aws_iam_role" "lambda_role" {
  name               = "lambda_role"
  description        = "this role gives permission to execute lambda function and create logs in cloudwatch"
  assume_role_policy = local.trust_policy_lambda
}

//adding AWSLambdaExecute  policy to lambda role
resource "aws_iam_role_policy_attachment" "attach-lambda-exec-role" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = "arn:aws:iam::aws:policy/AWSLambdaExecute"
}

//packaging the python code to .zip
data "archive_file" "zip_the_python_code" {
  type        = "zip"
  source_dir  = "${path.module}/python/"
  output_path = "${path.module}/python/python.zip"
}

//creating lambda function
resource "aws_lambda_function" "lambda" {
  function_name = "alert-log"
  role          = aws_iam_role.lambda_role.arn
  filename      = "${path.module}/python/python.zip"
  handler       = "index.lambda_handler"
  runtime       = "python3.8"
}

// creating cloudwatch log group for the function
resource "aws_cloudwatch_log_group" "alert_manager_logs" {
  name = "/aws/lambda/${aws_lambda_function.lambda.function_name}"
}

// creating the api
resource "aws_apigatewayv2_api" "api" {
  name          = "api-alert"
  protocol_type = "HTTP"
}

//deploy the api
resource "aws_apigatewayv2_stage" "dev_stg" {
  api_id = aws_apigatewayv2_api.api.id
  name   = "dev_stg"
  auto_deploy = true
}

//integrating http endpoint with lambda function
resource "aws_apigatewayv2_integration" "api_integration" {
  api_id           = aws_apigatewayv2_api.api.id
  integration_uri           = aws_lambda_function.lambda.invoke_arn
  integration_type = "AWS_PROXY"
  integration_method        = "POST"
}

resource "aws_apigatewayv2_route" "post_http" {
  api_id    = aws_apigatewayv2_api.api.id
  route_key = "ANY /alert-manager"
  target = "integrations/${aws_apigatewayv2_integration.api_integration.id}"
}

//attaching permissions to allow api gateway execute lambda functions
resource "aws_lambda_permission" "api_gw_permission" {
  statement_id = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn = "${aws_apigatewayv2_api.api.execution_arn}/*/*"
}

//creating log metric filter
resource "aws_cloudwatch_log_metric_filter" "alert-metric-1" {
  name           = "metric-1"
  pattern        = " \"[INFO]\" "
  log_group_name = aws_cloudwatch_log_group.alert_manager_logs.name

  metric_transformation {
    name      = "CallCountApi"
    namespace = "AlertMetricFilters"
    value     = "1"
  }
}

// creating alarm
resource "aws_cloudwatch_metric_alarm" "alarm_1" {
  alarm_name                = "alarm_prometheus"
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = 1
  metric_name               = "CallCountApi"
  namespace                 = "AlertMetricFilters"
  period                    = 60
  statistic                 = "Sum"
  threshold                 = 0
  alarm_description         = "This metric monitors api calls received in logs"
  insufficient_data_actions = []
  actions_enabled     = "true"
  //alarm_actions       = [aws_ssmincidents_response_plan.response_plan_1.arn]
}